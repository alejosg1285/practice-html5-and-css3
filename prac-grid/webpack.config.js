var path = require('path');
var MiniCssExtractPlugin = require("mini-css-extract-plugin");
var CleanWebpackPlugin = require('clean-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
var UglifyJsPlugin = require('uglifyjs-webpack-plugin');
var CopyWebpackPlugin = require('copy-webpack-plugin')
var webpack = require('webpack');

module.exports = {

    entry: './src/index.js',
    watch: true,
    //mode: 'development',
    //devtool: 'inline-source-map',
    output: {
        filename: 'js/app.js',
        path: path.resolve(__dirname, 'dist')
    },
    watchOptions: {
        ignored: /node_modules/,
        poll: 1000
    },

    module: {
        rules: [{
                test: /\.(sa|sc|c)ss$/,
                use: [{
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                            // you can specify a publicPath here
                            // by default it use publicPath in webpackOptions.output
                            publicPath: '../',
                            url: false,
                        }
                    },
                    'css-loader',
                    'sass-loader'
                ]
            },
            {
                test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        name: '[name].[ext]',
                        outputPath: 'fonts/'
                    }
                }]
            },
            {
                test: /\.(png|jp(e*)g|svg)$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        name: '[name].[ext]',
                        outputPath: 'images/',
                        /**/
                        emitFile: false
                    }
                }]
            },
            {
                test: /\.(png|jp(e*)g|svg)$/,
                use: [{
                    loader: 'url-loader',
                    options: {
                        limit: 8000, // Convert images < 8kb to base64 strings
                        ///name: 'images/[hash]-[name].[ext]'
                    }
                }]
            },
            /*{
                test: /\.html$/,
                use: 'raw-loader'
            },
            {
                test: /\.(jpe?g|png|gif)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            publicPath: 'assets/images/'
                        }
                    }
                ]
            },*/
        ],
    },

    plugins: [
        //new webpack.HotModuleReplacementPlugin(),
        new HtmlWebpackPlugin({
            template: './src/index.html'
        }),
        new MiniCssExtractPlugin({
            // Options similar to the same options in webpackOptions.output
            // both options are optional
            filename: "css/app.css",
            chunkFilename: "css/app.css",
            url: false,
        }),
        new CleanWebpackPlugin(['dist']),
        new CopyWebpackPlugin([{
                from: 'src/**/*.jpg',
                to: 'images/',
                flatten: true,
            },
            {
                from: 'src/**/*.svg',
                to: 'images/',
                flatten: true,
            },
            {
                from: 'src/**/*.png',
                to: 'images/',
                flatten: true,
            },
            {
                from: 'src/**/*.ttf',
                to: 'fonts/',
                flatten: true,
            },
            {
                from: 'src/fonts/*.svg',
                to: 'fonts/',
                flatten: true,
            },
            {
                from: 'src/**/*.eot',
                to: 'fonts/',
                flatten: true,
            },
            {
                from: 'src/**/*.css',
                to: 'css/',
                flatten: true,
            }
        ])
        //new UglifyJSPlugin(),
    ],
    optimization: {
        minimizer: [
            new UglifyJsPlugin({
                cache: true,
                parallel: true,
                sourceMap: true // set to true if you want JS source maps
            }),
            new OptimizeCSSAssetsPlugin({})
        ],
        /*splitChunks: {
            cacheGroups: {
                styles: {
                    name: 'styles',
                    test: /\.(sa|sc|c)ss$/,
                    chunks: 'all',
                    enforce: true
                }
            }
        }*/
    },
    devServer: {
        contentBase: path.join(__dirname, "/src"),
        compress: true,
        inline: true,
        port: 8888,
        open: true,
        publicPath: "/dist/",
        hot: true,
        watchOptions: {
            poll: true
        }
    },
    /*stats: { colors: true }*/
}

/*const webpack = require('webpack');
const path = require('path');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin')

module.exports = function(env) {
  return ({
    devtool: env === 'production' ? 'source-map' : 'cheap-eval-source-map',
    entry: './src/index.js',
    output: {
      filename: '[name].bundle.js',
      path: path.resolve(__dirname, 'dist')
    },
    module: {
      rules: [
        {
          test: /\.css$/,
          use: ExtractTextPlugin.extract({
            use: 'css-loader',
            fallback: 'style-loader'
          })
        },
        {
          test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2)$/,
          loader: 'url-loader',
          options: {
            limit: 10000
          }
        }
      ]
    },
    plugins: [
      new UglifyJSPlugin({sourceMap: true}),
      new ExtractTextPlugin('styles.css'),
      new HtmlWebpackPlugin({
        template: path.join(__dirname, 'public', 'index.html'),
      }),
      new CleanWebpackPlugin(['dist'])
    ]
  });
}*/